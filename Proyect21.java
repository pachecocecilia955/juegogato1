
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * PROYECTO JUEGO DEL GATO
 * @version 0.0.2
 * @author PACHECO CANUL CECILIA GABRIELA
 * @author TAMAY DE LOS SANTOS MONICA ESTEFANIA
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author FACULTAD DE INGENIERIA
 * @author LICENCIATURA COMO INGENIERO EN SISTEMAS COMPUTACIONALES
 * @author 2B
 * @author ASIGNATURA: LENGUAJE DE PROGRAMACIÓN 1
 * @author PROFESOR:CAAMAL DZULU EDGAR DAVID
 * @author 8/06/2020
 */


public class Proyect2 extends javax.swing.JFrame {

       String equis = "X";
       
       int contador;
       int counter;
       String jugadorX;
       String jugadorO;
       int partidas;
      
       /**
        * Valida quien sera el siguiente jugador.
        */
      
       int turno=-1;
    
   
    
    /**
     * Creates new form Proyect2
     */
    
      
       
       /**
       * CONSTRUCTOR
       */
       
     
       public Proyect2() {   // constructor
        initComponents();
    
    nombres();
  }

    public void nombres(){
    
    
    jugadorX = JOptionPane.showInputDialog(this,"Nombre del jugador");
    lb1.setText(jugadorX);
   
    jugadorO = JOptionPane.showInputDialog(this,"Nombre del jugador");
    lb2.setText(jugadorO);
    
    }
    
    
    public void ganador (){
    
        String uno = jButton1.getText();
        String dos = jButton2.getText();
        String tres = jButton3.getText();
        String cuatro = jButton4.getText();
        String cinco = jButton5.getText();
        String seis = jButton6.getText();
        String siete = jButton7.getText();
        String ocho = jButton8.getText();
        String nueve = jButton9.getText();
        
        
       
        
        
         if(uno.equals("X")&&dos.equals("X")&&tres.equals("X")){
            ganoX();
        }
        if(cuatro.equals("X")&&cinco.equals("X")&&seis.equals("X")){
            ganoX();
        }
        if(siete.equals("X")&&ocho.equals("X")&&nueve.equals("X")){
            ganoX();
        }if(uno.equals("X")&&cuatro.equals("X")&&siete.equals("X")){
            ganoX();
        }
        if(dos.equals("X")&&cinco.equals("X")&&ocho.equals("X")){
            ganoX();
        }
        if(tres.equals("X")&&seis.equals("X")&&nueve.equals("X")){
            ganoX();
        }
        if(uno.equals("X")&&cinco.equals("X")&&nueve.equals("X")){
            ganoX();
        }
        if(tres.equals("X")&&cinco.equals("X")&&siete.equals("X")){
            ganoX();
        
        }
    


//---------------------------------------------------------------------------------  
        
        
        if(uno.equals("O")&&dos.equals("O")&&tres.equals("O")){
            ganoO();
        }
        if(cuatro.equals("O")&&cinco.equals("O")&&seis.equals("O")){
            ganoO();
        }
        if(siete.equals("O")&&ocho.equals("O")&&nueve.equals("O")){
            ganoO();
        }if(uno.equals("O")&&cuatro.equals("O")&&siete.equals("O")){
            ganoO();
        }
        if(dos.equals("O")&&cinco.equals("O")&&ocho.equals("O")){
            ganoO();
        }
        if(tres.equals("O")&&seis.equals("O")&&nueve.equals("O")){
            ganoO();
        }
        if(uno.equals("O")&&cinco.equals("O")&&nueve.equals("O")){
            ganoO();
        }
        if(tres.equals("O")&&cinco.equals("O")&&siete.equals("O")){
            ganoO();
        }
        
    }
        
        
       
public void tablas(){
    

        String uno = jButton1.getText();
        String dos = jButton2.getText();
        String tres = jButton3.getText();
        String cuatro = jButton4.getText();
        String cinco = jButton5.getText();
        String seis = jButton6.getText();
        String siete = jButton7.getText();
        String ocho = jButton8.getText();
        String nueve = jButton9.getText();
        
       if(uno!=""&&dos !=""&&tres !=""&& cuatro !=""
                &&cinco !="" && seis !="" && siete !="" && ocho !="" & nueve !=""){

            
           
           /**
            * Valida los juegos empatados.
            */
           
           JOptionPane.showMessageDialog(null, "Empate");
           nuevojuego();

           contador += 1;
           String contador1 = String.valueOf(contador);
           campo3.setText(contador1);

       
       }
           

           }

/**
 * Valida quien es el ganador ganoX.
 */


public void ganoX(){
    
     JOptionPane.showMessageDialog(null,jugadorX,"Felicidades, ganaste",1);
        nuevojuego();
    
    contador +=1;
   
    String contador1 = String.valueOf(contador);
    

    campo1.setText(contador1);

    partidas += 1;

    String partidas1 = String.valueOf(partidas);

    campo4.setText(partidas1);

    if (partidas == 5) {

        JOptionPane.showMessageDialog(null, "Fin del juego");

        System.exit(0);

}

}
    
   
/**
 * Valida quien es el ganador ganoO.
 */


public void ganoO (){
    
     JOptionPane.showMessageDialog(null, jugadorO, "Felicidades, ganaste", 2);

    nuevojuego();

    counter += 1;

    String counter1 = String.valueOf(counter);

    campo2.setText(counter1);

    partidas += 1;

    String partidas1 = String.valueOf(partidas);

    campo4.setText(partidas1);

    if (partidas == 5) {

        JOptionPane.showMessageDialog(null, "Fin del juego");

        System.exit(0);

    
    }
    
   
    }

    
    
    public void nuevojuego (){
    
    
    jButton1.setText("");
    jButton2.setText("");
    jButton3.setText("");
    jButton4.setText("");
    jButton5.setText("");
    jButton6.setText("");
    jButton7.setText("");
    jButton8.setText("");
    jButton9.setText("");
    
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lb5 = new javax.swing.JLabel();
        campo5 = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        lb1 = new javax.swing.JLabel();
        campo1 = new javax.swing.JTextField();
        lb2 = new javax.swing.JLabel();
        campo2 = new javax.swing.JTextField();
        lb3 = new javax.swing.JLabel();
        campo4 = new javax.swing.JTextField();
        lb4 = new javax.swing.JLabel();
        campo3 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        lb5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lb5.setText("TURNO");

        campo5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 153, 153));

        jPanel1.setLayout(new java.awt.GridLayout(3, 3));

        jButton1.setBackground(new java.awt.Color(0, 153, 153));
        jButton1.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);

        jButton2.setBackground(new java.awt.Color(153, 153, 0));
        jButton2.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);

        jButton3.setBackground(new java.awt.Color(0, 153, 153));
        jButton3.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);

        jButton4.setBackground(new java.awt.Color(153, 153, 0));
        jButton4.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4);

        jButton5.setBackground(new java.awt.Color(0, 153, 153));
        jButton5.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton5);

        jButton6.setBackground(new java.awt.Color(153, 153, 0));
        jButton6.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton6);

        jButton7.setBackground(new java.awt.Color(0, 153, 153));
        jButton7.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton7);

        jButton8.setBackground(new java.awt.Color(153, 153, 0));
        jButton8.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton8);

        jButton9.setBackground(new java.awt.Color(0, 153, 153));
        jButton9.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton9);

        jButton10.setText("Borrar juego");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        jButton11.setText("Salir");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        lb1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lb1.setText("JUGADOR X");

        campo1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        campo1.setText("0");

        lb2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lb2.setText("JUGADOR O");

        campo2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        campo2.setText("0");

        lb3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lb3.setText("PARTIDAS");

        campo4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        campo4.setText("0");

        lb4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lb4.setText("EMPATES");

        campo3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        campo3.setText("0");

        jLabel1.setFont(new java.awt.Font("Century751 SeBd BT", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 0, 0));
        jLabel1.setText("JUEGO DEL GATO");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("TURNO");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 336, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 80, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(campo2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(campo1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(campo3, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(campo4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)))
                        .addGap(61, 61, 61))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButton11)
                        .addGap(51, 51, 51))))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lb3, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(180, 180, 180)
                            .addComponent(lb4, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(lb1, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lb2, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(45, 45, 45)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(34, 34, 34)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lb4)
                            .addComponent(campo3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lb3)
                            .addComponent(campo4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(campo1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lb1))
                        .addGap(36, 36, 36)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lb2)
                            .addComponent(campo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(45, 45, 45)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton10)
                            .addComponent(jButton11))))
                .addGap(43, 43, 43))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        jButton1.setText(equis);

       
        /**
         * Valida quien sera el siguiente jugador.
         */
        
        
        turno = 0;

        if (equis.equals("X")) {

            equis = "O";

            /**
             * Valida al jugador.
             */
            
            jLabel3.setText(jugadorO);

        } else {

            equis = "X";

            /**
             * Valida al jugador.
             */
            
            jLabel3.setText(jugadorX);
        }

        ganador();

        tablas();


        
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
          jButton2.setText(equis);
        turno = 0;

        if (equis.equals("X")) {
            equis = "O";

            jLabel3.setText(jugadorO);

        } else {

            equis = "X";

            jLabel3.setText(jugadorX);
        }
        ganador();

        tablas();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        jButton3.setText(equis);
       
         turno=0;
       
         if (equis.equals("X")){
          equis = "O";
         
        jLabel3.setText(jugadorO);
        }else{
            
            equis = "X";
          
        jLabel3.setText(jugadorX);
         
         }
        ganador();
        
        tablas();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        jButton4.setText(equis);

        turno = 0;

        if (equis.equals("X")) {
            equis = "O";

            jLabel3.setText(jugadorO);
        } else {

            equis = "X";

            jLabel3.setText(jugadorX);

        }
        ganador();

        tablas();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        jButton5.setText(equis);

        turno = 0;

        if (equis.equals("X")) {
            equis = "O";

            jLabel3.setText(jugadorO);

        } else {

            equis = "X";

            jLabel3.setText(jugadorX);

        }
        ganador();

        tablas();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        jButton6.setText(equis);

        turno = 0;

        if (equis.equals("X")) {
            equis = "O";
            jLabel3.setText(jugadorO);

        } else {

            equis = "X";

            jLabel3.setText(jugadorX);

        }
        ganador();

        tablas();
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        jButton7.setText(equis);

        turno = 0;

        if (equis.equals("X")) {
            equis = "O";

            jLabel3.setText(jugadorO);

        } else {

            equis = "X";
            jLabel3.setText(jugadorX);

        }
        ganador();

        tablas();
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
        jButton8.setText(equis);

        turno = 0;

        if (equis.equals("X")) {
            equis = "O";

            jLabel3.setText(jugadorO);

        } else {

            equis = "X";

            jLabel3.setText(jugadorX);

        }
        ganador();

        tablas();
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        // TODO add your handling code here
        jButton9.setText(equis);

        turno = 0;

        if (equis.equals("X")) {

            equis = "O";

            jLabel3.setText(jugadorO);

        } else {

            equis = "X";

            jLabel3.setText(jugadorX);

        }
        ganador();

        tablas();
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // TODO add your handling code here:
        
        nuevojuego();
        contador = 0;
        partidas = 0;
        counter = 0;
       
        String contador1 = String.valueOf(contador);
        campo1.setText(contador1);
        
        
        String counter1 = String.valueOf(counter);
        campo2.setText(counter1);
        
        
        String partidas1=String.valueOf(partidas);
        campo4.setText(counter1);
        
        
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        // TODO add your handling code here:
        
        System.exit(0);
        
        
    }//GEN-LAST:event_jButton11ActionPerformed

    /**
     * @param args the command line arguments
     */
    
    /**
     * 
     * @param args 
     */
    
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        
        
        
        
        
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Proyect2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Proyect2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Proyect2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Proyect2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Proyect2().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField campo1;
    private javax.swing.JTextField campo2;
    private javax.swing.JTextField campo3;
    private javax.swing.JTextField campo4;
    private javax.swing.JTextField campo5;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lb1;
    private javax.swing.JLabel lb2;
    private javax.swing.JLabel lb3;
    private javax.swing.JLabel lb4;
    private javax.swing.JLabel lb5;
    // End of variables declaration//GEN-END:variables

   
    }

