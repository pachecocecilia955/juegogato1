
package cat1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author personal
 */
public class Cat1 extends JFrame implements ActionListener  {

    
    JButton iniciar;
    JButton tablero;
    String jugador1, jugador2;
    int turno = -1;
    JLabel mensaje;
    Color colorB;
    
   
    public Cat1(){
    
        this.setLayout(null);
        mensaje = new JLabel ("¡BIENVENIDO AL JUEGO DEL GATO!");
        mensaje.setBounds(150,40,200,30);
        this.add(mensaje);
      iniciar = new JButton("Iniciar");
        iniciar.setBounds(175,350,150,30);
        iniciar.addActionListener(this);
        this.add(iniciar);
       JButton [][] tablero = new JButton [3][3];
    
        for (int i = 0; i < 3; i++){
           for (int j = 0; j < 3; j++){
            tablero[i][j] = new JButton();
            tablero[i][j].setBounds((i+1)*80,(j+1)*80,80,80);
            this.add(tablero[i][j]);
            tablero[i][j].addActionListener(this);
        }
    
   }
       
        colorB = tablero[0][0].getBackground();
    
    
    }
         
    
    
     public static void main(String[] args) {
        
        Cat1 ventana = new Cat1();
        ventana.setDefaultCloseOperation(3);
        ventana.setSize(500, 500);
        ventana.setLocationRelativeTo(null);
        ventana.setResizable(false);
        ventana.setTitle("Cat´s game");
        ventana.setVisible(true);
          
        
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        if(ae.getSource()==iniciar){
            turno = 0;
       
            JOptionPane.showMessageDialog(this,"Iniciando el juego");
    
           jugador1 = JOptionPane.showInputDialog(this,"Escribir nombre del jugador");
    
           jugador2 = JOptionPane.showInputDialog(this,"Escribir nombre del jugador");
    
    mensaje.setText("Turno del jugador" + jugador1);
    
    limpiar();
    
   
        } else {
        
        JButton boton = (JButton)ae.getSource();
            
        if (turno == 0){
         
            if (boton.getText().equals("")){
         
                boton.setBackground(Color.cyan);
         
                boton.setText("X");
         
                boton.setEnabled(false);
         
                turno = 1;
         
                mensaje.setText("Turno del jugador" + jugador2);
         }
        
        
        
        
        
        } else {
              
        if (turno == 0){
         
            if (boton.getText().equals("")){
         
                boton.setBackground(Color.white);
         
                boton.setText("O");
         
                boton.setEnabled(false);
         
                turno = 0;
            
         mensaje.setText("Turno del jugador" + jugador1);
         
                        }
                 }
        }
    

  
        
        
  

    